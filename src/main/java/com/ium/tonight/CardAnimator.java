package com.ium.tonight;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;

public class CardAnimator {

    private Activity activity;
    private ArrayList<CardView> cardList=new ArrayList<>();

    public CardAnimator(Activity activity, View viewToInspect){

        this.activity=activity;

        ViewGroup cardContainer = (ViewGroup) viewToInspect.findViewWithTag("CARD_CONTAINER");

        if(cardContainer!=null)
            for(int i=0; i<cardContainer.getChildCount(); i++){

                if(cardContainer.getChildAt(i).getClass() == CardView.class)
                    cardList.add((CardView) cardContainer.getChildAt(i));
            }
    }

    public void animate(){

        //Cerco l'altezza del display in pixel
        final int heightPx = activity.getResources().getDisplayMetrics().heightPixels;

        int delay = 240;

        for(CardView card : cardList) {

            if(card.getVisibility()==View.VISIBLE) {

                int[] location = new int[2];
                card.getLocationOnScreen(location);

                if (location[1] > heightPx) {
                    break;
                }

                Animation a = AnimationUtils.loadAnimation(activity, R.anim.slide_up);
                a.setStartOffset(delay);

                a.setDuration(350);
                card.startAnimation(a);

                delay += 120;
            }
        }
    }
}
