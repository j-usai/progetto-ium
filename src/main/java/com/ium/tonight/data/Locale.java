package com.ium.tonight.data;


import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.ium.tonight.MyApp;

import org.json.JSONObject;

import java.util.ArrayList;

public class Locale implements OgettoRicercabile{

    public String nome;
    public String descrizione;
    public String mappa;
    public String indirizzo;
    public String citta;

    public Locale(JSONObject localeJson) throws Exception{

        //Setto i dati
        nome = localeJson.getString("nome");
        descrizione = localeJson.getString("descrizione");
        mappa = localeJson.getString("mappa");
        citta = localeJson.getString("citta");
        indirizzo = localeJson.getString("indirizzo");
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Locale localeObj = (Locale) obj;

        if(! this.nome.equals(localeObj.nome))
            return false;

        return true;
    }

    public String getTitolo(){

        return nome;
    }

    public String getDescrizione(){

        return descrizione;
    }

    public Drawable getDrawable(Activity activity){

        return MyApp.loadDrawableFromAsset("immagini/locale/"+mappa,activity);
    }

    public boolean matches(String ricerca){

        ricerca = ricerca.toLowerCase();

        if(nome.toLowerCase().contains(ricerca)){

            return true;
        }

        return false;
    }

    public ArrayList<Evento> getRelatedEvents(){

        ArrayList<Evento> array = new ArrayList<>();

        for(Evento e: MyApp.eventi){

            if(e.locale.equals(this))
                array.add(e);
        }

        return array;
    }

}