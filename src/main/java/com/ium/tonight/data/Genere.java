package com.ium.tonight.data;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.ium.tonight.MyApp;
import com.ium.tonight.R;

import java.util.ArrayList;

public class Genere implements OgettoRicercabile {

    public String nome;

    public Genere (String nome){

        this.nome=nome;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Genere genereObj = (Genere) obj;

        if(! this.nome.equals(genereObj.nome))
            return false;

        return true;
    }

    public String getTitolo(){

        return nome;
    }

    public String getDescrizione(){

        return "Genere Musicale.";
    }

    public Drawable getDrawable(Activity activity){

        return activity.getDrawable(R.drawable.drawable_genere);
    }

    public boolean matches(String ricerca){

        ricerca = ricerca.toLowerCase();

        if(nome.toLowerCase().contains(ricerca))
            return true;
        else
            return false;
    }

    public ArrayList<Evento> getRelatedEvents(){

        ArrayList<Evento> array = new ArrayList<>();

        for(Evento e: MyApp.eventi){

            if(e.generi.contains(this))
                array.add(e);
        }

        return array;
    }
}
