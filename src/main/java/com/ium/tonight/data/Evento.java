package com.ium.tonight.data;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.ium.tonight.MyApp;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Evento implements OgettoRicercabile {

    public static final SimpleDateFormat DATA_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    public String titolo;
    public String copertina;
    public GregorianCalendar data_inizio = (GregorianCalendar) Calendar.getInstance();
    public GregorianCalendar data_fine = (GregorianCalendar) Calendar.getInstance();
    public Locale locale;
    public String descrizione;
    public Organizzazione organizzazione;
    public ArrayList<Genere> generi = new ArrayList<>();
    public ArrayList<Artista> artisti = new ArrayList<>();

    public Evento(JSONObject eventoJson) throws Exception{

        //Setto le date
        data_inizio.setTime(Evento.DATA_FORMAT.parse(eventoJson.getString("data_inizio")));
        data_fine.setTime(Evento.DATA_FORMAT.parse(eventoJson.getString("data_fine")));

        //Setto il locale
        locale = MyApp.getLocaleAt(eventoJson.getInt("locale"));

        //Setto l'organizzazione
        organizzazione = MyApp.getOrganizzazioneAt(eventoJson.getInt("organizzazione"));

        //Setto i generi
        for(int i=0; i<eventoJson.getJSONArray("generi").length(); i++){

            generi.add(new Genere(eventoJson.getJSONArray("generi").getString(i)));
        }

        //Setto gli artisti
        for(int i=0; i<eventoJson.getJSONArray("artisti").length(); i++){

            int idArtista = eventoJson.getJSONArray("artisti").getInt(i);

            artisti.add(MyApp.getArtistaAt(idArtista));
        }

        //Setto gli altri dati
        titolo = eventoJson.getString("titolo");
        copertina = eventoJson.getString("copertina");
        descrizione = eventoJson.getString("descrizione");

    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Evento eventoObj = (Evento) obj;

        if(! this.titolo.equals(eventoObj.titolo))
            return false;

        return true;
    }

    public String getTitolo(){

        return titolo;
    }

    public String getDescrizione(){

        return descrizione;
    }

    public Drawable getDrawable(Activity activity){

        return null;
    }

    public boolean matches(String ricerca){

        ricerca = ricerca.toLowerCase();

        if(titolo.toLowerCase().contains(ricerca))
            return true;

        return false;
    }

    public ArrayList<Evento> getRelatedEvents(){

        ArrayList<Evento> array = new ArrayList<>();
        array.add(this);

        return array;
    }
}
