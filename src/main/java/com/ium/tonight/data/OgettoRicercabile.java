package com.ium.tonight.data;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;

public interface OgettoRicercabile {

    public String getTitolo();

    public String getDescrizione();

    public Drawable getDrawable(Activity activity);

    public boolean matches(String ricerca);

    public ArrayList<Evento> getRelatedEvents();

}
