package com.ium.tonight.data;


import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.ium.tonight.MyApp;

import org.json.JSONObject;

import java.util.ArrayList;

public class Organizzazione implements OgettoRicercabile{

    public String nome;
    public String descrizione;
    public String logo;
    public String sfondo;

    public Organizzazione(JSONObject organizzazioneJson) throws Exception{

        //Setto i dati
        nome = organizzazioneJson.getString("nome");
        descrizione = organizzazioneJson.getString("descrizione");
        logo = organizzazioneJson.getString("logo");
        sfondo = organizzazioneJson.getString("sfondo");
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Organizzazione organizzazioneObj = (Organizzazione) obj;

        if(! this.nome.equals(organizzazioneObj.nome))
            return false;

        return true;
    }

    public String getTitolo(){

        return nome;
    }

    public String getDescrizione(){

        return descrizione;
    }

    public Drawable getDrawable(Activity activity){

        return MyApp.loadDrawableFromAsset("immagini/organ/"+logo,activity);
    }

    public Drawable getDrawableSfondo(Activity activity){

        return MyApp.loadDrawableFromAsset("immagini/organ/"+sfondo,activity);
    }

    public boolean matches(String ricerca){

        ricerca = ricerca.toLowerCase();

        if(nome.toLowerCase().contains(ricerca)){

            return true;
        }

        return false;
    }

    public ArrayList<Evento> getRelatedEvents(){

        ArrayList<Evento> array = new ArrayList<>();

        for(Evento e: MyApp.eventi){

            if(e.organizzazione.equals(this))
                array.add(e);
        }

        return array;
    }
}
