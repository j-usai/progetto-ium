package com.ium.tonight.data;


import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.ium.tonight.MyApp;

import org.json.JSONObject;

import java.util.ArrayList;

public class Artista implements OgettoRicercabile{

    public String nome;
    public String descrizione;
    public String tipo;
    public String foto;

    public Artista(JSONObject artistaJson) throws Exception{

        //Setto i dati
        nome = artistaJson.getString("nome");
        descrizione = artistaJson.getString("descrizione");
        tipo = artistaJson.getString("tipo");
        foto = artistaJson.getString("foto");
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Artista artistaObj = (Artista) obj;

        if(! this.nome.equals(artistaObj.nome))
            return false;

        return true;
    }

    public String getTitolo(){

        return nome;
    }

    public String getDescrizione(){

        return descrizione;
    }

    public Drawable getDrawable(Activity activity){

        return MyApp.loadDrawableFromAsset("immagini/dj/"+foto,activity);
    }

    public boolean matches(String ricerca){

        ricerca = ricerca.toLowerCase();

        if(nome.toLowerCase().contains(ricerca)){

            return true;
        }

        return false;
    }

    public ArrayList<Evento> getRelatedEvents(){

        ArrayList<Evento> array = new ArrayList<>();

        for(Evento e: MyApp.eventi){

            if(e.artisti.contains(this))
                array.add(e);
        }

        return array;
    }
}
