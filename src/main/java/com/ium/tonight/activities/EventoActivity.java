package com.ium.tonight.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ium.tonight.MyApp;
import com.ium.tonight.R;
import com.ium.tonight.data.Artista;
import com.ium.tonight.data.Evento;
import com.ium.tonight.data.Genere;
import com.ium.tonight.data.Organizzazione;
import com.konifar.fab_transformation.FabTransformation;
import com.makeramen.roundedimageview.RoundedImageView;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import at.blogc.android.views.ExpandableTextView;

public class EventoActivity extends AppCompatActivity {

    public Evento evento = MyApp.eventi.get(0);

    public View main;

    public static boolean fabOpened=false;

    public static String listaScelta="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        int eventIndex = intent.getIntExtra("index",0);

        evento = MyApp.eventi.get(eventIndex);

        //Imposto il titolo
        getSupportActionBar().setTitle(evento.getTitolo());

        main=findViewById(R.id.main);

        ((ImageView) findViewById(R.id.image)).setImageDrawable(MyApp.getCopertinaEvento(evento.copertina, this));

        //Imposto i testi
        ((TextView) main.findViewWithTag("titolo")).setText(evento.titolo);
        ((TextView) main.findViewWithTag("luogo")).setText(evento.locale.nome +", " +evento.locale.citta);
        String dayOfWeek = DateFormatSymbols.getInstance().getShortWeekdays()[evento.data_inizio.get(Calendar.DAY_OF_WEEK)];
        ((TextView) main.findViewWithTag("day")).setText(evento.data_inizio.get(Calendar.DAY_OF_MONTH) +"");
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        ((TextView) main.findViewWithTag("mese")).setText(month_date.format(evento.data_inizio.getTime()));

        //Imposto l'orario esteso
        SimpleDateFormat format = new SimpleDateFormat("EEEE HH:mm");
        String dataEstesa = format.format(evento.data_inizio.getTime()) +" - " +format.format(evento.data_fine.getTime());
        ((TextView) main.findViewWithTag("orario_esteso")).setText(dataEstesa);

        //Imposto la descrizione
        ((ExpandableTextView) main.findViewWithTag("descrizione")).setText(evento.descrizione);

        //Imposto l'espansione della descrizione
        main.findViewWithTag("descrizione").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((ExpandableTextView)v).toggle();

                if(main.findViewById(R.id.toggle).getRotationX()==0)
                    main.findViewById(R.id.toggle).setRotationX(180);
                else
                    main.findViewById(R.id.toggle).setRotationX(0);
            }
        });

        main.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((ExpandableTextView) main.findViewWithTag("descrizione")).toggle();

                if(v.getRotationX()==0)
                    v.setRotationX(180);
                else
                    v.setRotationX(0);
            }
        });

        //Imposto i generi
        LinearLayout generiContainer = (LinearLayout) main.findViewWithTag("elenco_generi");

        for(Genere genere : evento.generi){

            View view = getLayoutInflater().inflate(R.layout.m_genere, null);
            generiContainer.addView(view);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
            params.width = LinearLayout.LayoutParams.WRAP_CONTENT;
            params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            params.rightMargin =(int) getResources().getDimension(R.dimen.genere_padding);

            ((TextView) view.findViewWithTag("GENERE")).setText(genere.nome);
        }

        //Imposto l'organizzazione
        final View organizzazione = main;

        ((TextView) organizzazione.findViewWithTag("NOME_ORGANIZZAZIONE")).setText(evento.organizzazione.getTitolo());

        ((RoundedImageView)organizzazione.findViewWithTag("IMG")).setImageDrawable(evento.organizzazione.getDrawable(this));
        ((ImageView)organizzazione.findViewWithTag("IMG_SFONDO")).setImageDrawable(evento.organizzazione.getDrawableSfondo(this));

        final View segui = organizzazione.findViewWithTag("SEGUI");
        final View nonSeguire = organizzazione.findViewWithTag("NON_SEGUIRE");

        if(MyApp.organizzazioniSeguite.contains(evento.organizzazione))
            segui.setVisibility(View.GONE);
        else
            nonSeguire.setVisibility(View.GONE);

        segui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyApp.organizzazioniSeguite.add((Organizzazione) v.getTag());
                segui.setVisibility(View.GONE);
                nonSeguire.setVisibility(View.VISIBLE);
            }
        });
        segui.setTag(evento.organizzazione);

        nonSeguire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyApp.organizzazioniSeguite.remove((Organizzazione) v.getTag());
                segui.setVisibility(View.VISIBLE);
                nonSeguire.setVisibility(View.GONE);
            }
        });
        nonSeguire.setTag(evento.organizzazione);


        //Imposto gli artisti
        LayoutInflater inflater = getLayoutInflater();

        LinearLayout artisti = (LinearLayout) main.findViewById(R.id.artisti);

        for(final Artista a : evento.artisti) {
            View artista = inflater.inflate(R.layout.m_artista, null);
            artisti.addView(artista);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) artista.getLayoutParams();
            params.rightMargin = (int) getResources().getDimension(R.dimen.sedici);

            ((RoundedImageView)artista.findViewWithTag("IMG")).setImageDrawable(a.getDrawable(this));
            ((TextView)artista.findViewWithTag("TITOLO")).setText(a.getTitolo());
            ((TextView)artista.findViewWithTag("SOTTOTITOLO")).setText(a.tipo);
        }

        //Imposto la mappa
        ((ImageView) main.findViewWithTag("mappa")).setImageDrawable(evento.locale.getDrawable(this));

        //Imposto i dati sul luogo
        ((TextView) main.findViewWithTag("posto")).setText(evento.locale.nome);
        ((TextView) main.findViewWithTag("indirizzo_posto")).setText(evento.locale.indirizzo);

        //Imposto il fab
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FabTransformation.with(v)
                        .transformTo(findViewById(R.id.view_prenotazione));

                fabOpened=true;

                //Disattivo il focus su alcune view
                findViewById(R.id.toggle).setClickable(false);
                findViewById(R.id.toggle).setFocusable(false);
                findViewById(R.id.expandableTextView).setClickable(false);
                findViewById(R.id.expandableTextView).setFocusable(false);

                //Porto in alto
                ((NestedScrollView)findViewById(R.id.main)).smoothScrollTo(0,0);
                ((AppBarLayout)findViewById(R.id.app_bar_layout)).setExpanded(false);

            }
        });

        /*findViewById(R.id.main2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(fabOpened) {

                    FabTransformation.with(findViewById(R.id.fab))
                            .transformFrom(findViewById(R.id.view_prenotazione));

                    fabOpened = false;

                    //Riattivo il focus su alcune view
                    findViewById(R.id.toggle).setClickable(true);
                    findViewById(R.id.toggle).setFocusable(true);
                    findViewById(R.id.expandableTextView).setClickable(true);
                    findViewById(R.id.expandableTextView).setFocusable(true);
                }
            }
        });*/

        findViewById(R.id.main2).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(fabOpened) {

                    FabTransformation.with(findViewById(R.id.fab))
                            .transformFrom(findViewById(R.id.view_prenotazione));

                    fabOpened = false;

                    //Riattivo il focus su alcune view
                    findViewById(R.id.toggle).setClickable(true);
                    findViewById(R.id.toggle).setFocusable(true);
                    findViewById(R.id.expandableTextView).setClickable(true);
                    findViewById(R.id.expandableTextView).setFocusable(true);

                    return true;
                }
                else
                    return false;

            }
        });

        final Activity act =this;

        //Pulsante prenotati
        findViewById(R.id.prenotati).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<String> liste = new ArrayList<String>();
                liste.add("Lista Miao");
                liste.add("Lista Wuani");
                liste.add("Lista Light");
                liste.add("Lista Night");
                liste.add("Lista Ramsey");

                new MaterialDialog.Builder(act)
                        .title("Scegli una lista")
                        .items(liste)
                        .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                                MyApp.prenotaEvento(evento, text+"");

                                new MaterialDialog.Builder(act)
                                        .title("Prenotazione effettuata")
                                        .titleGravity(GravityEnum.CENTER)
                                        .customView(R.layout.view_conferma, false)
                                        .positiveText("Vai al biglietto")
                                        .negativeText("Chiudi")
                                        .dismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialogInterface) {

                                                act.finish();
                                                Home.home.updateData();
                                            }
                                        })
                                        .onAny(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                                if(which==DialogAction.NEUTRAL || which==DialogAction.NEGATIVE){

                                                    act.finish();
                                                    Home.home.updateData();
                                                }

                                                if(which==DialogAction.POSITIVE){

                                                    act.finish();
                                                    Home.home.goTo(1);
                                                    Home.home.updateData();
                                                }
                                            }
                                        })
                                        .show();

                                return true;
                            }
                        })
                        .positiveText("Conferma")
                        .show();
            }
        });

    }

    @Override
    public void onBackPressed() {

        if(fabOpened) {

            FabTransformation.with(findViewById(R.id.fab))
                    .transformFrom(findViewById(R.id.view_prenotazione));

            fabOpened = false;

            //Riattivo il focus su alcune view
            findViewById(R.id.toggle).setClickable(true);
            findViewById(R.id.toggle).setFocusable(true);
            findViewById(R.id.expandableTextView).setClickable(true);
            findViewById(R.id.expandableTextView).setFocusable(true);
        }
        else
            super.onBackPressed();
    }
}
