package com.ium.tonight.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ium.tonight.MyApp;
import com.ium.tonight.R;
import com.ium.tonight.data.Evento;
import com.ium.tonight.fragments.BachecaFragment;
import com.ium.tonight.fragments.BigliettiFragment;
import com.ium.tonight.fragments.CercaFragment;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private Home.SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public boolean ticketOpened=false;

    public static Home home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        home=this;

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new Home.SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(4);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Intent intent = getIntent();
        boolean showTicketOnStart = intent.getBooleanExtra("showTicketOnStart", false);

        if(showTicketOnStart)
            mViewPager.setCurrentItem(1);
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        }else if(ticketOpened){

            closeTicket();

        } else if(((CercaFragment)((SectionsPagerAdapter)mViewPager.getAdapter()).getItem(2)).isSearching()){

            ((CercaFragment)((SectionsPagerAdapter)mViewPager.getAdapter()).getItem(2)).stopSearch();
        }
        else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public BachecaFragment bacheca = BachecaFragment.newInstance();
        public BigliettiFragment biglietti = BigliettiFragment.newInstance();
        public CercaFragment ricerca = CercaFragment.newInstance();

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return bacheca;
                case 1:
                    return biglietti;
                default:
                    return ricerca;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Bacheca";
                case 1:
                    return "Biglietti";
                case 2:
                    return "Cerca";
            }
            return null;
        }
    }

    public void showFriends(View view){

        int id;

        if(view.getTag().equals("1")){

            id = R.layout.view_friends_1;
        }
        else if(view.getTag().equals("2")){

            id = R.layout.view_friends_2;
        }
        else {

            id = R.layout.view_friends_3;
        }

        new MaterialDialog.Builder(this)
        .customView(id, false)
        .positiveText("Chiudi")
        .show();
    }

    public void showFriendsEvent(View view){

        int index;

        if(view.getTag().equals("1")){

            index = 1;
        }
        else if(view.getTag().equals("2")){

            index = 5;
        }
        else {

            index = 8;
        }

        Intent intent = new Intent(this, EventoActivity.class);
        intent.putExtra("index", index);
        this.startActivity(intent);
    }

    public void openTicket(final Evento e,String lista, String codiceBiglietto, int index){

        if(ticketOpened)
            return;

        //Imposto i testi
        ((TextView) findViewById(R.id.codicePrenotazione)).setText(codiceBiglietto);
        ((TextView) findViewById(R.id.lista_ticket)).setText(lista);

        View viewEvento= findViewById(R.id.ticket_content);

        //Imposto i testi
        ((TextView) viewEvento.findViewWithTag("titolo")).setText(e.titolo);
        ((TextView) viewEvento.findViewWithTag("luogo")).setText(e.locale.nome +", " +e.locale.citta);
        String dayOfWeek = DateFormatSymbols.getInstance().getShortWeekdays()[e.data_inizio.get(Calendar.DAY_OF_WEEK)];
        ((TextView) viewEvento.findViewWithTag("day")).setText(e.data_inizio.get(Calendar.DAY_OF_MONTH) +"");
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        ((TextView) viewEvento.findViewWithTag("mese")).setText(month_date.format(e.data_inizio.getTime()));
        //Imposto l'orario esteso
        SimpleDateFormat format = new SimpleDateFormat("EEEE HH:mm");
        String dataEstesa = format.format(e.data_inizio.getTime()) +" - " +format.format(e.data_fine.getTime());
        ((TextView) viewEvento.findViewWithTag("orario_esteso")).setText(dataEstesa);

        final Activity activity =this;

        findViewById(R.id.annulla).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new MaterialDialog.Builder(activity)
                    .title("Sei sicuro?")
                    .content("Vuoi davvero annullare la prenotazione all'evento?")
                    .positiveText("Elimina")
                    .negativeText("Annulla")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            MyApp.annullaPrenotazione(e);
                            closeTicket();
                            ((BigliettiFragment)((SectionsPagerAdapter)mViewPager.getAdapter()).getItem(1)).drawnCards();
                        }
                    })
                    .show();
            }
        });

        //Rendo visibile
        findViewById(R.id.layer_black).setVisibility(View.VISIBLE);
        findViewById(R.id.ticket_background).setVisibility(View.VISIBLE);
        findViewById(R.id.ticket_content).setVisibility(View.VISIBLE);

        Window window = getWindow();
        window.setStatusBarColor(Color.parseColor("#610c2d"));

        ticketOpened=true;

    }

    public void goTo(int i){

        mViewPager.setCurrentItem(i);
    }

    public void closeTicket(){

        if(ticketOpened){

            findViewById(R.id.layer_black).setVisibility(View.GONE);
            findViewById(R.id.ticket_background).setVisibility(View.GONE);
            findViewById(R.id.ticket_content).setVisibility(View.GONE);

            Window window = getWindow();
            window.setStatusBarColor(Color.TRANSPARENT);

            ticketOpened=false;
        }
    }

    @Override
    protected void onStart() {
        home=this;
        super.onStart();
        home=this;
    }

    public void updateData(){

        ((BachecaFragment)((SectionsPagerAdapter)mViewPager.getAdapter()).getItem(0)).drawnCards();
        ((BigliettiFragment)((SectionsPagerAdapter)mViewPager.getAdapter()).getItem(1)).drawnCards();
    }
}
