package com.ium.tonight;

import android.app.Activity;
import android.app.Application;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.util.Pair;

import com.ium.tonight.data.Artista;
import com.ium.tonight.data.Evento;
import com.ium.tonight.data.Genere;
import com.ium.tonight.data.Locale;
import com.ium.tonight.data.Organizzazione;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

public class MyApp extends Application {

    public static ArrayList<Evento> eventi = new ArrayList<>();

    public static ArrayList<Artista> artisti = new ArrayList<>();
    public static ArrayList<Locale> locali = new ArrayList<>();
    public static ArrayList<Organizzazione> organizzazioni = new ArrayList<>();
    public static ArrayList<Genere> generi = new ArrayList<>();

    public static ArrayList<Organizzazione> organizzazioniSeguite = new ArrayList<>();

    public static ArrayList<Pair<Evento,String>> prenotazioni = new ArrayList<>();

    @Override
    public void onCreate(){
        super.onCreate();

        JSONArray arrayArtisti;
        JSONArray arrayLocali;
        JSONArray arrayOrganizzazioni;

        JSONArray arrayEventi;

        try {
            arrayArtisti = loadJSONFromAsset().getJSONArray("artisti");
            arrayLocali = loadJSONFromAsset().getJSONArray("locali");
            arrayOrganizzazioni = loadJSONFromAsset().getJSONArray("organizzazioni");
            arrayEventi = loadJSONFromAsset().getJSONArray("eventi");

            for(int i=0; i<arrayArtisti.length(); i++){

                artisti.add(new Artista(arrayArtisti.getJSONObject(i)));
            }

            for(int i=0; i<arrayLocali.length(); i++){

                locali.add(new Locale(arrayLocali.getJSONObject(i)));
            }

            for(int i=0; i<arrayOrganizzazioni.length(); i++){

                organizzazioni.add(new Organizzazione(arrayOrganizzazioni.getJSONObject(i)));
            }

            for(int i=0; i<arrayEventi.length(); i++){

                eventi.add(new Evento(arrayEventi.getJSONObject(i)));
            }

            for(Evento e : eventi){

                for(Genere g : e.generi){

                    if(!MyApp.generi.contains(g))
                        MyApp.generi.add(g);
                }

            }

            organizzazioniSeguite.add(organizzazioni.get(0));

        }catch (Exception e){
            System.out.println(e);
        }

    }

    public JSONObject loadJSONFromAsset() {
        JSONObject json = null;
        try {
            InputStream is = getAssets().open("arrayelementi.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new JSONObject(new String(buffer, "UTF-8"));
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

    public static Drawable getCopertinaEvento(String file, Activity activity) {

        return loadDrawableFromAsset("immagini/"+file, activity);
    }

    public static Drawable loadDrawableFromAsset(String file, Activity activity) {
        Drawable drawable = null;
        try {
            drawable = Drawable.createFromStream(activity.getAssets().open(file), null);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

        return drawable;
    }

    public static Artista getArtistaAt(int index){

        return artisti.get(index);
    }

    public static Locale getLocaleAt(int index){

        return locali.get(index);
    }

    public static Organizzazione getOrganizzazioneAt(int index){

        return organizzazioni.get(index);
    }

    public static void prenotaEvento(Evento evento, String lista){

        if(eventi.contains(evento)){

            prenotazioni.add(new Pair<Evento, String>(evento,lista));
            eventi.remove(evento);
        }
    }

    public static void annullaPrenotazione(Evento evento){

        Pair<Evento,String> elimina = null;

        for(Pair<Evento,String> p : prenotazioni){

            if(p.first.equals(evento))
                elimina=p;
        }

        if(elimina!=null)
            prenotazioni.remove(elimina);
    }
}
