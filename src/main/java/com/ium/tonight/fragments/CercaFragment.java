package com.ium.tonight.fragments;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.ium.tonight.MyApp;
import com.ium.tonight.R;
import com.ium.tonight.activities.Home;
import com.ium.tonight.data.Artista;
import com.ium.tonight.data.Evento;
import com.ium.tonight.data.Genere;
import com.ium.tonight.data.Locale;
import com.ium.tonight.data.OgettoRicercabile;
import com.ium.tonight.data.Organizzazione;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CercaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CercaFragment extends Fragment {

    public View mainView;

    public ArrayList<OgettoRicercabile> data = new ArrayList<>();

    public String query = "";

    public CercaFragment() {
        // Required empty public constructor
    }

    public static CercaFragment newInstance() {
        CercaFragment fragment = new CercaFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Creo il set di dati
        for(OgettoRicercabile oggetto :MyApp.artisti)
            data.add(oggetto);

        for(OgettoRicercabile oggetto :MyApp.generi)
            data.add(oggetto);

        for(OgettoRicercabile oggetto :MyApp.locali)
            data.add(oggetto);

        for(OgettoRicercabile oggetto :MyApp.organizzazioni)
            data.add(oggetto);

        for(OgettoRicercabile oggetto :MyApp.eventi)
            data.add(oggetto);

        for(OgettoRicercabile o : data){

            System.out.println(o.getTitolo());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.mainView = inflater.inflate(R.layout.fragment_cerca, container, false);

        //Disegno i generi
        LinearLayout generi = (LinearLayout) mainView.findViewById(R.id.generi_search);

        for(final Genere g : MyApp.generi) {
            final View genere = inflater.inflate(R.layout.m_genere_big, null);
            generi.addView(genere);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) genere.getLayoutParams();
            params.rightMargin = (int) getResources().getDimension(R.dimen.genere_padding_big);

            ((TextView)genere.findViewWithTag("GENERE")).setText(g.nome);

            genere.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((FloatingSearchView) mainView.findViewById(R.id.floating_search_view)).setSearchText(g.getTitolo());
                    cerca();
                }
            });
        }

        //Disegno le organizzazioni
        LinearLayout organizzazioni = (LinearLayout) mainView.findViewById(R.id.organizzazioni_search);

        for(final Organizzazione o : MyApp.organizzazioni) {
            final View organizzazione = inflater.inflate(R.layout.card_search_org, null);
            organizzazioni.addView(organizzazione);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) organizzazione.getLayoutParams();
            params.rightMargin = (int) getResources().getDimension(R.dimen.card_padding_lateral);

            ((RoundedImageView)organizzazione.findViewWithTag("IMG")).setImageDrawable(o.getDrawable(getActivity()));
            ((TextView)organizzazione.findViewWithTag("TITOLO")).setText(o.getTitolo());

            final View segui = organizzazione.findViewWithTag("SEGUI");
            final View nonSeguire = organizzazione.findViewWithTag("NON_SEGUIRE");

            if(MyApp.organizzazioniSeguite.contains(o))
                segui.setVisibility(View.GONE);
            else
                nonSeguire.setVisibility(View.GONE);

            organizzazione.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((FloatingSearchView) mainView.findViewById(R.id.floating_search_view)).setSearchText(o.getTitolo());
                    cerca();
                }
            });

            segui.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    MyApp.organizzazioniSeguite.add((Organizzazione) v.getTag());
                    segui.setVisibility(View.GONE);
                    nonSeguire.setVisibility(View.VISIBLE);
                    Home.home.updateData();
                }
            });
            segui.setTag(o);

            nonSeguire.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    MyApp.organizzazioniSeguite.remove((Organizzazione) v.getTag());
                    segui.setVisibility(View.VISIBLE);
                    nonSeguire.setVisibility(View.GONE);
                    Home.home.updateData();
                }
            });
            nonSeguire.setTag(o);

        }

        //Disegno gli artisti
        LinearLayout artisti = (LinearLayout) mainView.findViewById(R.id.artisti_search);

        for(final Artista a : MyApp.artisti) {
            View artista = inflater.inflate(R.layout.card_search, null);
            artisti.addView(artista);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) artista.getLayoutParams();
            params.rightMargin = (int) getResources().getDimension(R.dimen.card_padding_lateral);

            ((RoundedImageView)artista.findViewWithTag("IMG")).setImageDrawable(a.getDrawable(getActivity()));
            ((TextView)artista.findViewWithTag("TITOLO")).setText(a.getTitolo());
            ((TextView)artista.findViewWithTag("SOTTOTITOLO")).setText(a.tipo);

            artista.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((FloatingSearchView) mainView.findViewById(R.id.floating_search_view)).setSearchText(a.getTitolo());
                    cerca();
                }
            });

        }

        //Disegno i locali
        LinearLayout locali = (LinearLayout) mainView.findViewById(R.id.locali_search);

        for(final Locale l : MyApp.locali) {
            View locale = inflater.inflate(R.layout.card_search, null);
            locali.addView(locale);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) locale.getLayoutParams();
            params.rightMargin = (int) getResources().getDimension(R.dimen.card_padding_lateral);

            ((RoundedImageView)locale.findViewWithTag("IMG")).setImageDrawable(l.getDrawable(getActivity()));
            ((TextView)locale.findViewWithTag("TITOLO")).setText(l.getTitolo());
            ((TextView)locale.findViewWithTag("SOTTOTITOLO")).setText(l.citta);

            locale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((FloatingSearchView) mainView.findViewById(R.id.floating_search_view)).setSearchText(l.getTitolo());
                    cerca();
                }
            });

        }

        //Imposto il funzionamento della ricerca
        ((FloatingSearchView) mainView.findViewById(R.id.floating_search_view)).setOnFocusChangeListener(new FloatingSearchView.OnFocusChangeListener() {
            @Override
            public void onFocus() {

            }

            @Override
            public void onFocusCleared() {

                cerca();
            }
        });


        return mainView;
    }

    public void cerca(){

        String newQuery = ((FloatingSearchView) mainView.findViewById(R.id.floating_search_view)).getQuery();

        if (! newQuery.equals(query))
            ((ScrollView)mainView.findViewById(R.id.main_scroll)).scrollTo(0,0);

        if(newQuery.equals("")){

            mainView.findViewById(R.id.suggerimenti).setVisibility(View.VISIBLE);
            mainView.findViewById(R.id.risultati).setVisibility(View.GONE);
            mainView.findViewById(R.id.nada).setVisibility(View.GONE);

            query=newQuery;
        }
        else if (! newQuery.equals(query)) {

            query = newQuery;

            mainView.findViewById(R.id.suggerimenti).setVisibility(View.GONE);
            mainView.findViewById(R.id.risultati).setVisibility(View.VISIBLE);
            mainView.findViewById(R.id.nada).setVisibility(View.GONE);

            OgettoRicercabile risultato = null;
            ArrayList<Evento> eventiCorrelati = null;

            for (OgettoRicercabile o : data) {

                if (o.matches(query)) {
                    risultato = o;
                    break;
                }
            }

            if (risultato != null) {

                //Compilo la scheda del risultato
                if (risultato.getClass() == Evento.class)
                    mainView.findViewById(R.id.risultato_principale).setVisibility(View.GONE);
                else {
                    mainView.findViewById(R.id.risultato_principale).setVisibility(View.VISIBLE);

                    View principale = mainView.findViewById(R.id.risultato_principale);

                    ((RoundedImageView)principale.findViewWithTag("IMG")).setImageDrawable(risultato.getDrawable(getActivity()));

                    ((TextView)principale.findViewById(R.id.nome_risultato)).setText(risultato.getTitolo());
                    ((TextView)principale.findViewById(R.id.descrizione_risultato)).setText(risultato.getDescrizione());
                }

                //Aggiungo gli eventi correlati
                eventiCorrelati = risultato.getRelatedEvents();

                LinearLayout contenitoreEventi = (LinearLayout) mainView.findViewById(R.id.eventi_correlati);

                contenitoreEventi.removeAllViews();

                for (Evento e : eventiCorrelati) {
                    BachecaFragment.drawnCardEvento(e, contenitoreEventi, getActivity());
                }
            }
            else {

                mainView.findViewById(R.id.risultati).setVisibility(View.GONE);
                mainView.findViewById(R.id.nada).setVisibility(View.VISIBLE);
            }
        }
    }

    public boolean isSearching(){

        if(!query.equals(""))
            return true;
        else
            return false;
    }

    public void stopSearch(){

        if(!query.equals("")){

            mainView.findViewById(R.id.suggerimenti).setVisibility(View.VISIBLE);
            mainView.findViewById(R.id.risultati).setVisibility(View.GONE);
            mainView.findViewById(R.id.nada).setVisibility(View.GONE);

            ((FloatingSearchView) mainView.findViewById(R.id.floating_search_view)).setSearchText("");

            query="";
        }
    }

}
