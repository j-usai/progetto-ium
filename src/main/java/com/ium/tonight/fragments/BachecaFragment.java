package com.ium.tonight.fragments;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ium.tonight.CardAnimator;
import com.ium.tonight.MyApp;
import com.ium.tonight.R;
import com.ium.tonight.activities.EventoActivity;
import com.ium.tonight.data.Evento;
import com.ium.tonight.data.Genere;
import com.makeramen.roundedimageview.RoundedImageView;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BachecaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BachecaFragment extends Fragment {

    private View mainView;

    private boolean showAll = true;

    public BachecaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BachecaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BachecaFragment newInstance() {
        BachecaFragment fragment = new BachecaFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.mainView = inflater.inflate(R.layout.fragment_bacheca, container, false);

        //Imposto l'onclick dei pulsanti di visualizzazione eventi
        mainView.findViewById(R.id.button_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showEventsByType(true);
            }
        });

        mainView.findViewById(R.id.button_followed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showEventsByType(false);
            }
        });

        drawnCards();

        return mainView;
    }

    public void drawnCards(){

        LinearLayout cardContainer = (LinearLayout) mainView.findViewWithTag("CARD_CONTAINER");
        boolean eventiDisegnati=false;

        //Svuoto il contenitore delle schede
        cardContainer.removeAllViews();

        //Inserisco le schede
        for (int i = 0; i < MyApp.eventi.size(); i++) {

            Evento e = MyApp.eventi.get(i);

            //Dopo la seconda scheda disegno quella degli amici
            if (showAll && i == 2) {

                View viewAmici = getActivity().getLayoutInflater().inflate(R.layout.card_eventi_suggeriti, null);
                cardContainer.addView(viewAmici);

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) viewAmici.getLayoutParams();
                params.width = LinearLayout.LayoutParams.MATCH_PARENT;
                params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                params.bottomMargin = (int) viewAmici.getResources().getDimension(R.dimen.card_padding_between);
            }

            if (showAll || MyApp.organizzazioniSeguite.contains(e.organizzazione)) {

                drawnCardEvento(e, cardContainer, getActivity());
                eventiDisegnati=true;
            }
        }

        if(eventiDisegnati) {

            new CardAnimator(getActivity(), cardContainer).animate();

            mainView.findViewWithTag("NADA").setVisibility(View.GONE);
            cardContainer.setVisibility(View.VISIBLE);
        }
        else {

            mainView.findViewWithTag("NADA").setVisibility(View.VISIBLE);
            cardContainer.setVisibility(View.GONE);
        }

    }

    public static void drawnCardEvento(Evento e, LinearLayout container, final Activity activity){

        View viewEvento = activity.getLayoutInflater().inflate(R.layout.card_evento, null);
        container.addView(viewEvento);

        //Imposto il listener per passare alla nuova activity
        viewEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, EventoActivity.class);
                intent.putExtra("index", (Integer)v.getTag());
                activity.startActivity(intent);
            }
        });
        viewEvento.setTag(MyApp.eventi.indexOf(e));

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) viewEvento.getLayoutParams();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        params.bottomMargin =(int) activity.getResources().getDimension(R.dimen.card_padding_between);

        RoundedImageView copertina = (RoundedImageView) viewEvento.findViewWithTag("COPERTINA");
        Drawable copertinaDrawable = MyApp.getCopertinaEvento(e.copertina, activity);

        copertina.setImageDrawable(copertinaDrawable);

        //Imposto i testi
        ((TextView) viewEvento.findViewWithTag("titolo")).setText(e.titolo);
        ((TextView) viewEvento.findViewWithTag("luogo")).setText(e.locale.nome +", " +e.locale.citta);
        String dayOfWeek = DateFormatSymbols.getInstance().getShortWeekdays()[e.data_inizio.get(Calendar.DAY_OF_WEEK)];
        ((TextView) viewEvento.findViewWithTag("day_of_week")).setText(dayOfWeek);
        ((TextView) viewEvento.findViewWithTag("day")).setText(e.data_inizio.get(Calendar.DAY_OF_MONTH) +"");
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        ((TextView) viewEvento.findViewWithTag("mese")).setText(month_date.format(e.data_inizio.getTime()));

        //Imposto i generi
        LinearLayout generiContainer = (LinearLayout) viewEvento.findViewWithTag("elenco_generi");

        for(Genere genere : e.generi){

            View view = activity.getLayoutInflater().inflate(R.layout.m_genere, null);
            generiContainer.addView(view);

            params = (LinearLayout.LayoutParams) view.getLayoutParams();
            params.width = LinearLayout.LayoutParams.WRAP_CONTENT;
            params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            params.rightMargin =(int) activity.getResources().getDimension(R.dimen.genere_padding);

            ((TextView) view.findViewWithTag("GENERE")).setText(genere.nome);
        }

    }

    public void showEventsByType(boolean all){

        TextView allButton =(TextView) mainView.findViewById(R.id.button_all);
        TextView followedButton =(TextView) mainView.findViewById(R.id.button_followed);

        ImageView center =(ImageView) mainView.findViewById(R.id.segmented_control_center);

        if(all){
            followedButton.setBackgroundColor(Color.TRANSPARENT);
            allButton.setBackground(getActivity().getDrawable(R.drawable.segmented_control_left_selection));
            followedButton.setTextColor(getActivity().getResources().getColor(R.color.segmented_control_color));
            allButton.setTextColor(getActivity().getResources().getColor(R.color.white));
            center.setRotationY(0);
            center.setRotationX(0);

            if(!showAll) {
                showAll = true;
                drawnCards();
            }
        }
        else {
            allButton.setBackgroundColor(Color.TRANSPARENT);
            followedButton.setBackground(getActivity().getDrawable(R.drawable.segmented_control_right_selection));
            allButton.setTextColor(getActivity().getResources().getColor(R.color.segmented_control_color));
            followedButton.setTextColor(getActivity().getResources().getColor(R.color.white));
            center.setRotationY(180);
            center.setRotationX(180);

            if(showAll) {
                showAll = false;
                drawnCards();
            }

        }

        System.out.println(showAll +" ---------------------------");

    }
}
