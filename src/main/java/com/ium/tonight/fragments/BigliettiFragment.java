package com.ium.tonight.fragments;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ium.tonight.CardAnimator;
import com.ium.tonight.MyApp;
import com.ium.tonight.R;
import com.ium.tonight.activities.EventoActivity;
import com.ium.tonight.activities.Home;
import com.ium.tonight.data.Evento;
import com.ium.tonight.data.Genere;
import com.makeramen.roundedimageview.RoundedImageView;

import java.math.BigInteger;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BigliettiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BigliettiFragment extends Fragment {

    private View mainView;

    public BigliettiFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BigliettiFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BigliettiFragment newInstance() {
        BigliettiFragment fragment = new BigliettiFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.mainView = inflater.inflate(R.layout.fragment_biglietti, container, false);

        drawnCards();

        return mainView;
    }

    public void drawnCards(){

        LinearLayout cardContainer = (LinearLayout) mainView.findViewWithTag("CARD_CONTAINER");
        boolean eventiDisegnati=false;

        //Svuoto il contenitore delle schede
        cardContainer.removeAllViews();

        //Inserisco le schede
        for (int i = 0; i < MyApp.prenotazioni.size(); i++) {

            Evento e = MyApp.prenotazioni.get(i).first;
            String lista = MyApp.prenotazioni.get(i).second;
            String codiceBiglietto = "H4" +e.titolo.charAt(0) +(int)e.titolo.charAt(2);

            drawnCardBiglietto(e, cardContainer, lista, codiceBiglietto, i, getActivity());
            eventiDisegnati=true;
        }

        if(eventiDisegnati) {

            new CardAnimator(getActivity(), cardContainer).animate();

            mainView.findViewWithTag("NADA").setVisibility(View.GONE);
            cardContainer.setVisibility(View.VISIBLE);
        }
        else {

            mainView.findViewWithTag("NADA").setVisibility(View.VISIBLE);
            cardContainer.setVisibility(View.GONE);
        }

    }

    public void drawnCardBiglietto(final Evento e, LinearLayout container, final String lista, final String codiceBiglietto, final int index, final Activity activity){

        View viewEvento = activity.getLayoutInflater().inflate(R.layout.card_biglietto, null);
        container.addView(viewEvento);

        //Imposto il listener per aprire il biglietto
        viewEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((Home)activity).openTicket(e,lista,codiceBiglietto,index);
            }
        });
        viewEvento.setTag(index);

        String newCodiceBiglietto="Cod. biglietto: "+codiceBiglietto;

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) viewEvento.getLayoutParams();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        params.bottomMargin =(int) activity.getResources().getDimension(R.dimen.card_padding_between);

        RoundedImageView copertina = (RoundedImageView) viewEvento.findViewWithTag("COPERTINA");
        Drawable copertinaDrawable = MyApp.getCopertinaEvento(e.copertina, activity);

        copertina.setImageDrawable(copertinaDrawable);

        //Imposto i testi
        ((TextView) viewEvento.findViewWithTag("titolo")).setText(e.titolo);
        ((TextView) viewEvento.findViewWithTag("luogo")).setText(e.locale.nome +", " +e.locale.citta);
        String dayOfWeek = DateFormatSymbols.getInstance().getShortWeekdays()[e.data_inizio.get(Calendar.DAY_OF_WEEK)];
        ((TextView) viewEvento.findViewWithTag("day")).setText(e.data_inizio.get(Calendar.DAY_OF_MONTH) +"");
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        ((TextView) viewEvento.findViewWithTag("mese")).setText(month_date.format(e.data_inizio.getTime()));

        ((TextView) viewEvento.findViewWithTag("lista")).setText(lista);
        ((TextView) viewEvento.findViewWithTag("codice_biglietto")).setText(newCodiceBiglietto);

        //Imposto l'orario esteso
        SimpleDateFormat format = new SimpleDateFormat("EEEE HH:mm");
        String dataEstesa = format.format(e.data_inizio.getTime()) +" - " +format.format(e.data_fine.getTime());
        ((TextView) viewEvento.findViewWithTag("orario_esteso")).setText(dataEstesa);

    }

}
